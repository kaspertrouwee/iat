import pygame
import RPi.GPIO as GPIO
import time

done = False

GPIO.setmode(GPIO.BOARD)

control_pinsL = [3, 5, 7, 11]
control_pinsR = [13, 15, 19, 21]

pygame.init()

# Initialize the joysticks.
pygame.joystick.init()

for pin in control_pinsL:
    GPIO.setup(pin, GPIO.OUT)
    GPIO.output(pin, 0)
for pin in control_pinsR:
    GPIO.setup(pin, GPIO.OUT)
    GPIO.output(pin, 0)

halfstep_seqL = [
    [1, 0, 0, 0],
    [1, 1, 0, 0],
    [0, 1, 0, 0],
    [0, 1, 1, 0],
    [0, 0, 1, 0],
    [0, 0, 1, 1],
    [0, 0, 0, 1],
    [1, 0, 0, 1]
]

halfstep_seqR = [
    [1, 0, 0, 0],
    [1, 1, 0, 0],
    [0, 1, 0, 0],
    [0, 1, 1, 0],
    [0, 0, 1, 0],
    [0, 0, 1, 1],
    [0, 0, 0, 1],
    [1, 0, 0, 1]
]

halfstep_seqL_achteruit = [
    [0, 0, 0, 1],
    [0, 0, 1, 1],
    [0, 0, 1, 0],
    [0, 1, 1, 0],
    [0, 1, 0, 0],
    [1, 1, 0, 0],
    [1, 0, 0, 0],
    [1, 0, 0, 1]
]

halfstep_seqR_achteruit = [
    [0, 0, 0, 1],
    [0, 0, 1, 1],
    [0, 0, 1, 0],
    [0, 1, 1, 0],
    [0, 1, 0, 0],
    [1, 1, 0, 0],
    [1, 0, 0, 0],
    [1, 0, 0, 1]
]


def achteruit():
    print("achteruit")
    for halfstep in range(8):
        for pin in range(4):
            GPIO.output(control_pinsL[pin], halfstep_seqL_achteruit[halfstep][pin])
            GPIO.output(control_pinsR[pin], halfstep_seqR_achteruit[halfstep][pin])
        time.sleep(0.00075)


def rechts():
    print("rechts")
    for halfstep in range(8):
        for pin in range(4):
            GPIO.output(control_pinsL[pin], halfstep_seqL[halfstep][pin])
        time.sleep(0.00075)


def links():
    print("links")
    for halfstep in range(8):
        for pin in range(4):
            GPIO.output(control_pinsR[pin], halfstep_seqR[halfstep][pin])
        time.sleep(0.00075)


def vooruit():
    print("vooruit")
    for halfstep in range(8):
        for pin in range(4):
            GPIO.output(control_pinsL[pin], halfstep_seqL[halfstep][pin])
            GPIO.output(control_pinsR[pin], halfstep_seqR[halfstep][pin])
        time.sleep(0.00075)


joystick_count = pygame.joystick.get_count()

while not done:
    #
    # EVENT PROCESSING STEP
    #
    # done = True  # Flag that we are done so we exit this loop.

    for i in range(joystick_count):
        joystick = pygame.joystick.Joystick(i)
        joystick.init()
        # Get the name from the OS for the controller/joystick.
        # name = joystick.get_name()
        # # Usually axis run in pairs, up/down for one, and left/right for
        # # the other.
        # axes = joystick.get_numaxes()
        #
        # for i in range(axes):
        #     axis = joystick.get_axis(i)
        buttons = joystick.get_numbuttons()

        for i in range(buttons):
            button = joystick.get_button(i)
            if button == 1:
                print(button)

        hats = joystick.get_numhats()
        # Hat position. All or nothing for direction, not a float like
        # get_axis(). Position is a tuple of int values (x, y).
        # (-1, 0) = left
        # (1, 0) = rechts
        # (0, 1) = up
        # (0, -1) = down
        for i in range(hats):
            hat = joystick.get_hat(i)
            if hat == (-1, 0):
                print("links")
                links()

            elif hat == (1, 0):
                print("rechts")
                rechts()

            elif hat == (0, 1):
                print("vooruit")
                vooruit()

            elif hat == (0, -1):
                print("achteruit")
                achteruit()

GPIO.cleanup()
