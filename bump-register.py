# imports are program code that are needed to run your program
import RPi.GPIO as GPIO  # import library for working with Raspberry's GPIO
import time  # needed to u

################################
# Global variables and set up  #
################################

# global variables
buttonPinL = 12  # this will be an input pin to which the button is attached
buttonPinR = 16
buttonPinB = 21
# in this case pin GPIO4 (which is pin number 7)
prev_stateL = 1  # set start state to 1 (button released)
prev_stateR = 1
prev_stateB = 1

# we're using the BCM pin layout of the Raspberry PI
GPIO.setmode(GPIO.BCM)

# set pin GPIO4 to be an input pin; this pin will read the button state
# activate pull down for pin GPIO4
GPIO.setup(buttonPinL, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(buttonPinR, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(buttonPinB, GPIO.IN, pull_up_down=GPIO.PUD_UP)

############
# Run code #
############

# initialize event
eventL = 1
eventR = 1
EventB = 1

print("Button register")

# keep on executing this loop forever (until someone stops the program)
while True:

    # read the current button state by reading pin GPIO4 on the Raspberry PI
    # the curr_state can be '0' (if button pressed) or '1' (if button released)
    curr_stateL = GPIO.input(buttonPinL)
    curr_stateR = GPIO.input(buttonPinR)
    curr_stateB = GPIO.input(buttonPinB)

    # if state changed, take some actions
    if (curr_stateL != prev_stateL):  # state changed from '1' to '0' or from '0' to '1'
        if (curr_stateL == 1):  # button changed from pressed ('0') to released ('1')
            eventL = "released Left"
            print(eventL)  # print event to console
        else:  # button changed from released ('1') to pressed ('0')
            eventL = "pressed Left"  # print event to console
            print(eventL)
        prev_stateL = curr_stateL  # store current state

    elif (curr_stateR != prev_stateR):  # state changed from '1' to '0' or from '0' to '1'
        if (curr_stateR == 1):  # button changed from pressed ('0') to released ('1')
            eventR = "released Right"
            print(eventR)  # print event to console
        else:  # button changed from released ('1') to pressed ('0')
            eventR = "pressed Right"  # print event to console
            print(eventR)
        prev_stateR = curr_stateR  # store current state

    elif (curr_stateB != prev_stateB):  # state changed from '1' to '0' or from '0' to '1'
        if (curr_stateB == 1):  # button changed from pressed ('0') to released ('1')
            eventB = "released Back"
            print(eventB)  # print event to console
        else:  # button changed from released ('1') to pressed ('0')
            eventB = "pressed Back"  # print event to console
            print(eventB)
        prev_stateB = curr_stateB  # store current state

    time.sleep(0.02)  # sle
    # ep for a while, to prevent bouncing

# when exiting, reset all pins
GPIO.cleanup()
