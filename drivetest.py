# ======================================================================
# This class is a template for a killer kart. For
# Now it can only move (forward, backward, left, right,
# rotate in place). It has a function test_drive that
# sets out a course and tests all these functions.
# Commented next to test_drive are the movement predictions.
#
# TODO:
# add button effects
# add behavior
# optional: add optical effects
# optional: add audio effects
# ...
#
# =============== For indices (parameter in spin_wheel() ===============
# 100 indices = approx. 1 second
# 24 indices = approx. 1 cm of distance
# 512 indices = 1 full rotation of the wheel or approx. 20.1 cm of distance
# 640 indices = 180 degree turn of the kart or approx. 25.1 cm of distance
# 320 indices = 90 degree turn of the kart or approx. 12.6 cm of distance
# 160 indices = 45 degree turn of the kart or approx. 6.3 cm of distance
# ======================================================================

import RPi.GPIO as GPIO
import time
import pygame

# Initialize pygame
pygame.init()
# Initialize the joysticks.
pygame.joystick.init()

# READ IN BCM: THESE ARE THE GPIO NUMBERS
# NOT THE PIN NUMBERS ON THE RASPBERRY PI
gpio_pins_left = [3, 5, 7, 11]
gpio_pins_right = [13, 15, 19, 21]
active_coil_matrix = [ [1, 0, 0, 0],   # Sequence in which coils in the electric
                            [1, 1, 0, 0],   # motor are powered. The elements represent
                            [0, 1, 0, 0],   # the pins in gpio_pins_(left/right).
                            [0, 1, 1, 0],
                            [0, 0, 1, 0],
                            [0, 0, 1, 1],
                            [0, 0, 0, 1],
                            [1, 0, 0, 1] ]

# Initializes the GPIO pins connected to the motor as an output
def setup():
    GPIO.setmode(GPIO.BCM)
    gpio_pins = [2, 3, 4, 17, 27, 22, 10, 9]
    for pin in gpio_pins:
        GPIO.setup(pin, GPIO.OUT)

def move():
    setup()
    # Loop until the user clicks the close button.
    done = False
    joystick_count = pygame.joystick.get_count()
    # -------- Main Program Loop -----------
    while not done:
        # Get the joysticks and arrow keys
        for i in range(joystick_count):
            joystick = pygame.joystick.Joystick(i)
            joystick.init()
            hats = joystick.get_numhats()

        # Hat position. All or nothing for direction, not a float like
        # get_axis(). Position is a tuple of int values (x, y).
        # (-1, 0 ) = left
        # (1, 0) = right
        # (0, 1) = up
        # (0, -1) = down
        for x in range(hats):
            hat = joystick.get_hat(x)
            # Rotate to the left
            if hat == (-1, 0):
                print(hat)
                drive("rotate left")
            # Rotate to the right
            elif hat == (1, 0):
                drive("rotate right")
            # Move forward
            elif hat == (0, 1):
                drive("forward")
            # Move backward
            elif hat == (0, -1):
                drive("back")

        GPIO.cleanup()

        # Get all the buttons of the controller
        buttons = joystick.get_numbuttons()

        # If the 'O' button is pressed, stop the program.
        for i in range(buttons):
            button = joystick.get_button(i)
            if (i == 2 and button == 1):
                done=True

# Makes a wheel spin in a certain direction (clockwise / counterclockwise)
def drive(direction):
    setup()
    for step in range(8):
        for pin in range(4):
            if direction.lower() == "forward":
                GPIO.output(gpio_pins_left[pin], active_coil_matrix[4 - step][pin])         # Sequences like normal
                GPIO.output(gpio_pins_right[pin], active_coil_matrix[step][pin])            # Sequences in reverse, since right wheel is mirrored: net forward
            elif direction.lower() == "back":
                GPIO.output(gpio_pins_left[pin], active_coil_matrix[step][pin])             # Reverse of the
                GPIO.output(gpio_pins_right[pin], active_coil_matrix[4 - step][pin])        # forward movement
            elif direction.lower() == "rotate right":
                GPIO.output(gpio_pins_left[pin], active_coil_matrix[4 - step][pin])         # Both wheels moving opposite direction,
                GPIO.output(gpio_pins_right[pin], active_coil_matrix[4 - step][pin])        # causing the kart to turn in its place (right)
            elif direction.lower() == "rotate left":
                GPIO.output(gpio_pins_left[pin], active_coil_matrix[step][pin])             # Both wheels moving opposite direction,
                GPIO.output(gpio_pins_right[pin], active_coil_matrix[step][pin])            # Causing the kart to turn in its place (left)
            time.sleep(0.00075)

if __name__== "__main__":
    move()