import RPi.GPIO as GPIO
import time

GPIO.setmode(GPIO.BOARD)

control_pinsL = [3, 5, 7, 11]
control_pinsR = [13, 15, 19, 21]

for pin in control_pinsR:
  GPIO.setup(pin, GPIO.OUT)
  GPIO.output(pin, 0)
for pin in control_pinsL:
   GPIO.setup(pin, GPIO.OUT)
   GPIO.output(pin, 0)

halfstep_seqL = [
  [0,0,0,1],
  [0,0,1,1],
  [0,0,1,0],
  [0,1,1,0],
  [0,1,0,0],
  [1,1,0,0],
  [1,0,0,0],
  [1,0,0,1]
]

halfstep_seqR = [
  [1,0,0,0],
  [1,1,0,0],
  [0,1,0,0],
  [0,1,1,0],
  [0,0,1,0],
  [0,0,1,1],
  [0,0,0,1],
  [1,0,0,1]
]
for i in range(512):
  for halfstep in range(8):
    for pin in range(4):
      GPIO.output(control_pinsL[pin], halfstep_seqL[halfstep][pin])
      GPIO.output(control_pinsR[pin], halfstep_seqR[halfstep][pin])
    time.sleep(0.001)
GPIO.cleanup()
