import random

# Standaard vooruit, Links en rechts, Achteruit
# Als bumpers = 1, 3 sec achteruit, kwartslag draai
# randomizer voor movement
# Alsie dede is stilstaan zijkant
# Blijft rijden tenzij 1 ballon stuk is
# startup = cirkeltje draaien
# lichtjes connecten
# externe controller connect

# 512 range van halfstep_seq = 4 sec

sec = 128
richting = 0

def vooruit():
    duur = random.randint(64,512)
    while duur != 0:
        print("Vooruit")

def achteruit():
    duur = random.randint(64,128)
    while duur != 0:
        print("Achteruit")
        
def links():
    duur = random.randint(64,256)
    while duur != 0:
        print("Links")

def rechts():
    duur = random.randint(64,256)
    while duur != 0:
        print("Rechts")

def stuurrichting():
    richting = random.randint(1,4)
    print(richting)
    if richting == 1:
        vooruit()
    elif richting == 2:
        achteruit()
    elif richting == 3:
        links()
    elif richting == 4:
        rechts()
    

def main():
    stuurrichting()
    




if __name__ == "__main__":
    main()
