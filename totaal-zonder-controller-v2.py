import random
import RPi.GPIO as GPIO
import time
from gpiozero import LED
import pygame

global joystick
hat = 0

# Standaard vooruit, Links en rechts, Achteruit
# Als bumpers = 1, 3 sec achteruit, kwartslag draai
# randomizer voor movement
# Alsie dede is stilstaan zijkant
# Blijft rijden tenzij 1 ballon stuk is
# startup = cirkeltje draaien
# lichtjes connecten
# externe controller connect

# sec = 128
# richting = 0

GPIO.setmode(GPIO.BOARD)

control_pinsL = [3, 5, 7, 11]
control_pinsR = [13, 15, 19, 21]

for pin in control_pinsL:
    GPIO.setup(pin, GPIO.OUT)
    GPIO.output(pin, 0)
for pin in control_pinsR:
    GPIO.setup(pin, GPIO.OUT)
    GPIO.output(pin, 0)

halfstep_seqL = [
    [1, 0, 0, 0],
    [1, 1, 0, 0],
    [0, 1, 0, 0],
    [0, 1, 1, 0],
    [0, 0, 1, 0],
    [0, 0, 1, 1],
    [0, 0, 0, 1],
    [1, 0, 0, 1]
]

halfstep_seqR = [
    [1, 0, 0, 0],
    [1, 1, 0, 0],
    [0, 1, 0, 0],
    [0, 1, 1, 0],
    [0, 0, 1, 0],
    [0, 0, 1, 1],
    [0, 0, 0, 1],
    [1, 0, 0, 1]
]

halfstep_seqL_achteruit = [
    [0, 0, 0, 1],
    [0, 0, 1, 1],
    [0, 0, 1, 0],
    [0, 1, 1, 0],
    [0, 1, 0, 0],
    [1, 1, 0, 0],
    [1, 0, 0, 0],
    [1, 0, 0, 1]
]

halfstep_seqR_achteruit = [
    [0, 0, 0, 1],
    [0, 0, 1, 1],
    [0, 0, 1, 0],
    [0, 1, 1, 0],
    [0, 1, 0, 0],
    [1, 1, 0, 0],
    [1, 0, 0, 0],
    [1, 0, 0, 1]
]

# global variables
buttonPinL = 32  # this will be an input pin to which the button is attached
buttonPinR = 36
buttonPinB = 40
# in this case pin GPIO4 (which is pin number 7)
prev_stateL = 1  # set start state to 1 (button released)
prev_stateR = 1
prev_stateB = 0

# we're using the BCM pin layout of the Raspberry PI

# set pin GPIO4 to be an input pin; this pin will read the button state
# activate pull down for pin GPIO4
GPIO.setup(buttonPinL, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(buttonPinR, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(buttonPinB, GPIO.IN, pull_up_down=GPIO.PUD_UP)

# led mummers
rechts_led = LED(7)
middel_led = LED(8)
links_led = LED(6)

middel_led.on()


def dood():
    middel_led.off()
    for i in range(1500):
        for halfstep in range(8):
            for pin in range(4):
                GPIO.output(control_pinsL[pin], halfstep_seqL[halfstep][pin])
                GPIO.output(control_pinsR[pin], halfstep_seqR[halfstep][pin])
            time.sleep(0.001)
    exit()


def botsingL():
    for i in range(512):
        for halfstep in range(8):
            for pin in range(4):
                GPIO.output(control_pinsL[pin], halfstep_seqL_achteruit[halfstep][pin])
                GPIO.output(control_pinsR[pin], halfstep_seqR_achteruit[halfstep][pin])
            time.sleep(0.001)

    for i in range(512):
        for halfstep in range(8):
            for pin in range(4):
                links_led.on()
                GPIO.output(control_pinsR[pin], halfstep_seqR[halfstep][pin])
                links_led.off()
            time.sleep(0.001)


def botsingR():
    for i in range(512):
        for halfstep in range(8):
            for pin in range(4):
                GPIO.output(control_pinsL[pin], halfstep_seqL_achteruit[halfstep][pin])
                GPIO.output(control_pinsR[pin], halfstep_seqR_achteruit[halfstep][pin])
            time.sleep(0.001)

    for i in range(512):
        for halfstep in range(8):
            for pin in range(4):
                rechts_led.on()
                GPIO.output(control_pinsL[pin], halfstep_seqL[halfstep][pin])
                rechts_led.off()
            time.sleep(0.001)


def achteruit():
    for i in range(256):
        for halfstep in range(8):
            for pin in range(4):
                GPIO.output(control_pinsL[pin], halfstep_seqL_achteruit[halfstep][pin])
                GPIO.output(control_pinsR[pin], halfstep_seqR_achteruit[halfstep][pin])
            time.sleep(0.001)


def rechts():
    for i in range(512):
        for halfstep in range(8):
            for pin in range(4):
                rechts_led.on()
                GPIO.output(control_pinsL[pin], halfstep_seqL[halfstep][pin])
                rechts_led.off()
            time.sleep(0.001)


def links():
    for i in range(512):
        for halfstep in range(8):
            for pin in range(4):
                links_led.on()
                GPIO.output(control_pinsR[pin], halfstep_seqR[halfstep][pin])
                links_led.off()
            time.sleep(0.001)


def vooruit():
    for i in range(512):
        for halfstep in range(8):
            for pin in range(4):
                GPIO.output(control_pinsL[pin], halfstep_seqL[halfstep][pin])
                GPIO.output(control_pinsR[pin], halfstep_seqR[halfstep][pin])
            time.sleep(0.001)


def vooruit_random(draai_duur):
    for i in range(draai_duur):
        for halfstep in range(8):
            for pin in range(4):
                GPIO.output(control_pinsL[pin], halfstep_seqL[halfstep][pin])
                GPIO.output(control_pinsR[pin], halfstep_seqR[halfstep][pin])
            time.sleep(0.001)


def achteruit_random(draai_duur):
    for i in range(draai_duur):
        for halfstep in range(8):
            for pin in range(4):
                GPIO.output(control_pinsL[pin], halfstep_seqL_achteruit[halfstep][pin])
                GPIO.output(control_pinsR[pin], halfstep_seqR_achteruit[halfstep][pin])
            time.sleep(0.001)


def links_random(draai_duur):
    for i in range(draai_duur):
        for halfstep in range(8):
            for pin in range(4):
                GPIO.output(control_pinsR[pin], halfstep_seqR[halfstep][pin])
                GPIO.output(control_pinsL[pin], halfstep_seqL_achteruit[halfstep][pin])
            time.sleep(0.001)


def rechts_random(draai_duur):
    for i in range(draai_duur):
        for halfstep in range(8):
            for pin in range(4):
                GPIO.output(control_pinsL[pin], halfstep_seqL[halfstep][pin])
                GPIO.output(control_pinsR[pin], halfstep_seqR_achteruit[halfstep][pin])
            time.sleep(0.001)


def stuurrichting():
    richting = random.randint(1, 6)
    if richting == 1 or richting == 5 or richting == 6:
        vooruit()
    elif richting == 2:
        achteruit()
    elif richting == 3:
        links()
    elif richting == 4:
        rechts()


pygame.init()

# Loop until the user clicks the close button.
z = 0

# Initialize the joysticks.
pygame.joystick.init()


def controller(z):
    done = False
    while not done:
        #
        # EVENT PROCESSING STEP
        #
        # Possible joystick actions: JOYAXISMOTION, JOYBALLMOTION, JOYBUTTONDOWN,
        # JOYBUTTONUP, JOYHATMOTION
        for event in pygame.event.get():  # User did something.
            if event.type == pygame.QUIT:  # If user clicked close.
                done = True  # Flag that we are done so we exit this loop.
            elif event.type == pygame.JOYBUTTONDOWN:
                print("Joystick button pressed.")
            elif event.type == pygame.JOYBUTTONUP:
                print("Joystick button released.")

        #
        # DRAWING STEP
        #

        # Get count of joysticks.
        joystick_count = pygame.joystick.get_count()

        # For each joystick:
        for i in range(joystick_count):
            joystick = pygame.joystick.Joystick(i)
            joystick.init()

            # Get the name from the OS for the controller/joystick.
            name = joystick.get_name()

            # Usually axis run in pairs, up/down for one, and left/right for
            # the other.
            axes = joystick.get_numaxes()

            for i in range(axes):
                axis = joystick.get_axis(i)

            buttons = joystick.get_numbuttons()

            for i in range(buttons):
                button = joystick.get_button(i)

            hats = joystick.get_numhats()

            # Hat position. All or nothing for direction, not a float like
            # get_axis(). Position is a tuple of int values (x, y).
            # (-1, 0) = left
            # (1, 0) = rechts
            # (0, 1) = up
            # (0, -1) = down
            for i in range(hats):
                hat = joystick.get_hat(i)
                print(hat)
                if z == 3:
                    autonoom()
                elif hat == (-1, 0):
                    links()
                    print(z)
                    z = z + 1
                elif hat == (1, 0):
                    rechts()
                    print(z)
                    z = z +1

                elif hat == (0, 1):
                    vooruit()
                    print(z)
                    z = z + 1

                elif hat == (0, -1):
                    achteruit()
                    print(z)
                    z = z + 1



# ---------------------------------------------------------------------------------------

def autonoom():
    ############
    # Run code #
    ############

    # initialize event
    eventL = 1
    eventR = 1
    eventB = 1

    # in this case pin GPIO4 (which is pin number 7)
    prev_stateL = 1  # set start state to 1 (button released)
    prev_stateR = 1
    prev_stateB = 1

    # read the current button state by reading pin GPIO4 on the Raspberry PI
    # the curr_state can be '0' (if button pressed) or '1' (if button released)
    curr_stateL = GPIO.input(buttonPinL)
    curr_stateR = GPIO.input(buttonPinR)
    curr_stateB = GPIO.input(buttonPinB)
    x = False
    # keep on executing this loop forever (until someone stops the program)
    # if state changed, take some actions
    # x = False    # while x == False:
    #     if (curr_stateL != prev_stateL):  # state changed from '1' to '0' or from '0' to '1'
    #         if (curr_stateL == 1):  # button changed from pressed ('0') to released ('1')
    #             print("press the button to start")
    #         else:  # button changed from released ('1') to pressed ('0')
    #             eventL = "pressed Left"  # print event to console
    #             x = True
    #         prev_stateL = curr_stateL  # store current state
    while x == False:
        #
        # EVENT PROCESSING STEP
        #
        # Possible joystick actions: JOYAXISMOTION, JOYBALLMOTION, JOYBUTTONDOWN,
        # JOYBUTTONUP, JOYHATMOTION
        for event in pygame.event.get():  # User did something.
            if event.type == pygame.QUIT:  # If user clicked close.
                done = True  # Flag that we are done so we exit this loop.
            elif event.type == pygame.JOYBUTTONDOWN:
                print("Joystick button pressed.")
            elif event.type == pygame.JOYBUTTONUP:
                print("Joystick button released.")

        #
        # DRAWING STEP
        #

        # Get count of joysticks.
        joystick_count = pygame.joystick.get_count()

        # For each joystick:
        for i in range(joystick_count):
            joystick = pygame.joystick.Joystick(i)
            joystick.init()

            # Get the name from the OS for the controller/joystick.
            name = joystick.get_name()

            # Usually axis run in pairs, up/down for one, and left/right for
            # the other.
            axes = joystick.get_numaxes()

            for i in range(axes):
                axis = joystick.get_axis(i)

            buttons = joystick.get_numbuttons()

            for i in range(buttons):
                button = joystick.get_button(i)

            hats = joystick.get_numhats()
            print(hats)

            # Hat position. All or nothing for direction, not a float like
            # get_axis(). Position is a tuple of int values (x, y).
            # (-1, 0) = left
            # (1, 0) = rechts
            # (0, 1) = up
            # (0, -1) = down
            for i in range(hats):
                hat = joystick.get_hat(i)
                print(hat)
                if hat == (-1, 0):
                    x = True

                elif hat == (1, 0):
                    x = True

                elif hat == (0, 1):
                    x = True

                elif hat == (0, -1):
                    x = True

    while x == True:

        curr_stateL = GPIO.input(buttonPinL)
        curr_stateR = GPIO.input(buttonPinR)
        curr_stateB = GPIO.input(buttonPinB)

        if (curr_stateB != prev_stateB):  # state changed from '1' to '0' or from '0' to '1'
            if (curr_stateB == 0):  # button changed from pressed ('0') to released ('1')
                eventB = "released Back"
                print(eventB)  # print event to console
                stuurrichting()
            else:  # button changed from released ('1') to pressed ('0')
                eventB = "pressed Back"  # print event to console
                print(eventB)
                dood()
            prev_stateB = curr_stateB  # store current state

        elif (curr_stateL != prev_stateL):  # state changed from '1' to '0' or from '0' to '1'
            if (curr_stateL == 1):  # button changed from pressed ('0') to released ('1')
                eventL = "released Left"
                print(eventL)  # print event to console
                stuurrichting()
            else:  # button changed from released ('1') to pressed ('0')
                eventL = "pressed Left"  # print event to console
                print(eventL)
                botsingL()
            prev_stateL = curr_stateL  # store current state

        elif (curr_stateR != prev_stateR):  # state changed from '1' to '0' or from '0' to '1'
            if (curr_stateR == 1):  # button changed from pressed ('0') to released ('1')
                eventR = "released Right"
                print(eventR)  # print event to console
                stuurrichting()
            else:  # button changed from released ('1') to pressed ('0')
                eventR = "pressed Right"  # print event to console
                print(eventR)
                botsingR()
            prev_stateR = curr_stateR  # store current state

        else:
            stuurrichting()
        time.sleep(0.02)  # sleep for a while, to prevent bouncing


# when exiting, reset all pins


# ---------------------------------------------------------------------------------------


def main():
    autonoom()


if __name__ == "__main__":
    main()

GPIO.cleanup()
pygame.quit()
